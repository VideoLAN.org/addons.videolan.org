<?php

namespace Addons\Validator;
use Zend\Validator\AbstractValidator;

class AddonStatus extends AbstractValidator
{
    const OUT_OF_RANGE = 0;
    var $values = array(
    'invalid',
    'waiting',
    'valid',
    'updated',
    'junk');

    protected $messageTemplates = array(
        self::OUT_OF_RANGE => "'%value%' is an invalid addon status"
    );

    public function isValid($value)
    {
        if (!in_array($value, $this->values))
        {
            $this->setValue($value);
            $this->error(self::OUT_OF_RANGE);
            return false;
        }
        return true;
    }
}
