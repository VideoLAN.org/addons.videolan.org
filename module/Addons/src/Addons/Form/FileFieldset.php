<?php

namespace Addons\Form;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\InputFilter\Input;

class FileFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name, $options);

        $this->setLabel('File');

        $this->add(array(
            'name' => 'fileupload',
            'type' => 'file',
            'options' => array(
                'label' => 'File'
            ),
        ));
        
        $this->add(array(
            'name' => 'filename',
            'type' => 'text',
            'options' => array(
                'label' => 'File Name'
            ),
        ));
        
        $this->add(array(
            'name' => 'oldname',
            'type' => 'hidden',
        ));
        
        $this->add(array(
            'name' => 'type',
            'type' => 'select',
            'options' => array(
                'label' => 'Type',
                'value_options' => array_flip( array('Extension'=> 'extension', 'Skin' => 'skin', 'Playlist' => 'playlist', 'Service Discovery' => 'discovery', 'Interface' => 'interface', 'Meta fetcher' => 'meta') ),
            ),
        ));
        
        $this->add(array(
            'name' => 'delete',
            'type' => 'button',
            'options' => array(
                'label' => 'Remove',
            ),
            'attributes' => array(
                'class' => 'small alert round',
                'onclick' => 'return deletefile($(this));'
            )
        ));

    }

    /**
     * @return array
     \*/
    public function getInputFilterSpecification()
    {
        return array(
            'delete' => array(
                'required' => false,
            ),
            'filename' => array(
                'required'  => true,
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => 100
                        )
                    ),
                    array(
                         'name' => 'Regex',
                         'options' => array(
                             'pattern' => "/^[A-z0-9]+[A-z0-9_\/\-\. ]+$/"
                          ) 
                    )
                ),
                'filters'   => array(
                    array('name' => 'StringTrim'),
                    array('name' => 'StripTags'),
                ),
            ),
            'fileupload' => array(
                'required'  => false,
                'validators' => array(
                    
                ),
                'filters'   => array(
                    // Custom Renamer
                  //  array('name' => 'myFileRename'),
                ),
            )
        );
    }
}