<?php

namespace Addons\Form;

use Zend\Form\Element;
use ZfcBase\Form\ProvidesEventsForm;
use Addons\Model\Addon;
use Zend\Filter;
use Zend\InputFilter\Input;
use Zend\InputFilter\FileInput;

class EditAddonForm extends ProvidesEventsForm
{
    public function __construct()
    {
        parent::__construct();

         $this->add(array(
            'name' => 'thumbnail',
            'options' => array(
                'label' => 'Thumbnail',
            ),
            'attributes' => array(
                'type' => 'file'
            ),
        ));

        $input = new FileInput('thumbnail');
        $input->getValidatorChain()->addValidator( new \Zend\Validator\File\UploadFile() );
        $input->setRequired( false );
        $this->getInputFilter()->add( $input );

        $this->add(array(
            'name' => 'name',
            'required' => true,
            'options' => array(
                'label' => 'Addon Name',
            ),
            'attributes' => array(
                'type' => 'text'
            ),
        ));

        $input = new Input('name');
        $input->getFilterChain()->attach( new Filter\StringTrim() );
        $input->getValidatorChain()->attach( Addon::getValidator('name') );
        $this->getInputFilter()->add( $input );

        $this->add(array(
            'name' => 'summary',
            'required' => true,
            'options' => array(
                'label' => 'Summary',
            ),
            'attributes' => array(
                'type' => 'text'
            ),
        ));

        $input = new Input('summary');
        $input->getFilterChain()->attach( new Filter\StringTrim() );
        $input->getValidatorChain()->attach( Addon::getValidator('summary') );
        $this->getInputFilter()->add( $input );

        $textareaElement = new Element\Textarea('description');
        $textareaElement->setAttributes( array('rows' => '40') );
        $textareaElement->setLabel('Description');
        $this->add( $textareaElement );
        $input = new Input('description');
        $this->getInputFilter()->add( $input );

        $this->add(array(
            'name' => 'version',
            'options' => array(
                'label' => 'Version',
            ),
            'attributes' => array(
                'type' => 'text',
                'maxlength' => '8'
            ),
        ));

        $input = new Input('version');
        $input->setRequired( false );
        $input->getFilterChain()->attach( new Filter\StringTrim() );
        $input->getValidatorChain()->attach( Addon::getValidator('version') );
        $this->getInputFilter()->add( $input );

        $select = new Element\Select('type');
        $select->setLabel('Type');
        $select->setValueOptions( array_flip( array('Extension'=> 'extension', 'Skin' => 'skin', 'Playlist' => 'playlist', 'Service Discovery' => 'discovery', 'Interface' => 'interface', 'Meta fetcher' => 'meta' ) ));
        $this->add($select);

        $input = new Input('type');
        $input->setRequired( true );
        $this->getInputFilter()->add( $input );

        $submitElement = new Element\Button('submit');
        $submitElement
            ->setLabel('Commit changes')
            ->setAttributes(array(
                'type'  => 'submit',
            ));
        $this->add($submitElement, array(
            'priority' => -100,
        ));



    }
}
