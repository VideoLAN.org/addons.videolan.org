<?php
namespace Addons\Model;
use Zend\Permissions\Acl\Resource\ResourceInterface;

class Addon implements ResourceInterface
{
    public $uuid;
    public $type;
    public $name;
    public $downloads;
    public $score;
    public $version;
    public $image;
    public $imagedata;
    public $archive;
    public $summary;
    public $description;
    public $ref_userid;
    public $size;
    public $votes;
    public $status;

    public $resources = array();

    protected $fields = array('uuid', 'type', 'name', 'downloads', 'score', 'version', 'image', 'imagedata', 'archive', 'summary', 'description', 'ref_userid', 'size', 'votes', 'status');

    public function exchangeArray($data)
    {
        foreach( $this->fields as $v )
        {
            $this->$v = (isset($data[$v])) ? $data[$v] : null;
        }
        /* Handle blob returned as resource */
        if ( is_resource( $this->imagedata ) )
                $this->imagedata = stream_get_contents( $this->imagedata );
    }

    static public function getValidator( $type )
    {
        switch( $type )
        {
            case 'uuid':
                return new \Zend\Validator\Regex("/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/");
            case 'type':
                return new \Addons\Validator\AddonType;
            case 'status':
                return new \Addons\Validator\AddonStatus;
            case 'name':
                $chain = new \Zend\Validator\ValidatorChain();
                $chain->addByName( "StringLength", array('min'=>1, 'max'=>126) );
                $chain->addByName( "Regex", array( 'pattern' => "/^[A-z0-9_\-\. ]+$/" ) );
                return $chain;
            case 'summary':
                return new \Zend\Validator\StringLength( array('min'=>10, 'max'=>255) );
            case 'version':
                return new \Zend\Validator\Regex("/^[0-9\.]*$/");
            default:
                return null;
        }
    }

    public function validateAll( )
    {
        foreach( $this->fields as $v )
        {
            $validator = self::getValidator($v);
            if ( $validator && !$validator->isValid($this->$v) )
            {
                $messages = $validator->getMessages();
                    if ( count($messages) )
                        throw new \Exception( $v . ": " . array_shift($messages) );
                    else
                        throw new \Exception( $v . ": is invalid" );
            }
        }

        foreach( $this->resources as $res )
            $res->validateAll();

        if ( count( $this->resources ) < 1 )
            throw new \Exception( "No files in addon" );

        return true;
    }

    public function getResourceId()
    {
        return 'addon';
    }
}
