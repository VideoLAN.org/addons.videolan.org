<?php

namespace Addons\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Expression;

class AddonsFileTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function fetch( $uuid, $type = null )
    {
        $params = array();
        if ( !empty( $type ) )
            $params['type'] = $type;
        $params['ref_uuid'] = $uuid;
        $resultSet = $this->tableGateway->select( $params );

        return $resultSet;
    }
    
    public function getFile($uuid, $filename)
    {
        $rowset = $this->tableGateway->select(array('ref_uuid' => $uuid, 'filename'=>$filename));
        $row = $rowset->current();
        return $row;
    }

    public function deleteFile($uuid, $filename)
    {
        $this->tableGateway->delete( array('ref_uuid' => $uuid, 'filename'=> $filename ) );
    }

    public function saveFile(AddonFile $addonfile, $key_filename)
    {
        $filedata = $addonfile->data;
        if (is_resource($filedata) )
            $filedata = stream_get_contents($filedata);

        $data = array(
            'ref_uuid' => $addonfile->ref_uuid,
            'filename' => $addonfile->filename,
            'type' => $addonfile->type,
            'data' => new Expression("DECODE('" . base64_encode($filedata) . "', 'base64')")
        );

        if ( $this->getFile($addonfile->ref_uuid, $key_filename) )
        {
            $this->tableGateway->update($data, 
                    array('ref_uuid' => $addonfile->ref_uuid, 'filename' => $key_filename));
        }
        else
        {
            $this->tableGateway->insert($data);
        }
    }
}