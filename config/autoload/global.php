<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return array(
    // ...
    'view_helpers' => array(
        'invokables' => array(
            'formbutton' => 'ZurbFoundationFour\Form\View\Helper\FormButton',
            'formelementerrors' => 'ZurbFoundationFour\Form\View\Helper\FormElementErrors',
            'formmulticheckbox' => 'ZurbFoundationFour\Form\View\Helper\FormMultiCheckbox',
            'formradio'         => 'ZurbFoundationFour\Form\View\Helper\FormRadio',
        )
    ),
    
);
